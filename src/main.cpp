#include "Arduino.h"
#include "ESP8266WiFi.h"
#include <WiFiClient.h>
#include "ArduinoJson.h"

const char* ssid = "MARLAM";
const char* password = "luisdavid";
const int led = 13;

const char host[] = "192.168.0.24";
const char url[] = "/sensor/new/";
const int httpPort = 1337;

void setup() {
  pinMode(led, OUTPUT);
  digitalWrite(led, 0);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());


}

void loop() {
  if(WiFi.status() == WL_CONNECTED){
    WiFiClient client;

    while (!client.connect(host, httpPort)) {
      Serial.println("connection failed");
      delay(1000);
    }

    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    root["name"] = "wifiA";
    root["value"] = random(0, 100);

    char pstr[100];
    root.printTo(pstr, 100);
    Serial.println("informacion enviada: ");
    Serial.print(pstr);

    String data(pstr);

    client.print(
      String("POST ") + url + " HTTP/1.1\r\n" +
     "Host: " + host + "\r\n" +
     //"Connection: close\r\n" +
     "Content-Type: application/json\r\n" +
     "Content-Length: " + data.length() + "\r\n" +
     "\r\n" + // This is the extra CR+LF pair to signify the start of a body
     data + "\n");



     delay(500);
     if(client.available()){
       String response = client.readString();
       Serial.print(response);
     }




    Serial.println();

  }
  delay(5000);

}
